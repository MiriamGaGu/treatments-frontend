
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class NavBar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            open: false
        };
    }
    //Toggle function (open/close Drawer)
    toggleDrawer = () => {
        this.setState({
            open: !this.state.open
        })
    }

    render() {
        const AppBarStyles = {
            felx: 1
        };
        const ListItemTextStyle = {
            width: 200
        };

        return (

            <AppBar position="static">
                <Toolbar>
                    <IconButton onClick={this.toggleDrawer} color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="title" variant="h6" style={AppBarStyles} color="inherit" >
                        Muktek
                    </Typography>
                    <div className=''>
                        <Link to='/'>
                            <Typography variant="title" variant="h6" style={AppBarStyles} color="inherit" >
                                Home
                            </Typography>
                        </Link>
                        <Link to='/login'>
                            <Typography variant="title" variant="h6" style={AppBarStyles} color="inherit" >
                                Login
                        </Typography>
                        </Link>
                    </div>


                </Toolbar>
                <Drawer open={this.state.open} onClose={this.toggleDrawer}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.toggleDrawer}
                        onKeyDown={this.toggleDrawer}
                    >
                        <List>
                            <ListItem>
                                <ListItemText style={ListItemTextStyle} primary="Features" />
                            </ListItem>
                            <ListItem button>
                                <Link to='/users'>Users</Link>
                            </ListItem>
                            <Divider />
                            <ListItem button>
                                <Link to='/treatments'>Treatments</Link>
                            </ListItem>
                        </List>

                    </div>
                </Drawer>
            </AppBar>

        )
    };

}

NavBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBar);